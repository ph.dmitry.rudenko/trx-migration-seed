// simple express.js app

const express = require('express');
const bodyParser = require('body-parser');
const { User, sequelize } = require('./models');

const app = express();

app.use(bodyParser.json());

const Subscriptions = {
    free: {
        price: 20,
    },
    premium: {
        price: 50,
    },
    pro: {
        price: 100,
    }
}


app.get('/', (req, res) => {
    res.send('Hello World!');
})

const paySubscription = async (id, balance, subscription) => {
    return new Promise(async (resolve) => {
        setTimeout(async () => {
            if (balance < Subscriptions[subscription].price) {
                resolve(false);
            }

            resolve(true);
        }, 30000)
    })
}

app.post('/subscribe', async (req, res) => {
    const { id, subscription } = req.body;

    if (!id || !subscription) {
        res.status(400).send('Missing id or subscription');
        return;
    }

    const user = await User.findByPk(id);

    if (!user) {
        res.status(404).send('User not found');
        return;
    }

    if (user.balance <= 0) {
        res.status(400).send('Balance too low');
        return;
    }

    const trx = await sequelize.transaction();

    try {
        user.balance -= Subscriptions[subscription].price;

        await user.save();

        const pay = await paySubscription(id, user.balance, subscription);

        if (!pay) {
            await trx.rollback();
            res.status(400).send('Insufficient balance');
            return;
        }

        user.subscription = subscription;

        await user.save();

        await trx.commit();

        res.send('Subscription successful');
    } catch (error) {
        await trx.rollback();
        res.status(500).send('Something went wrong');
    }
})

// curl -X POST http://localhost:3000/subscribe -d '{"id": 1, "subscription": "free"}'

app.listen(3000, () => {
    console.log('Server started on port 3000');
});