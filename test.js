const axios = require('axios')

const main = async () => {
    try {
        const response = await axios({
            method: 'post',
            url: 'http://localhost:3000/subscribe',
            data: {
                id: 2,
                subscription: 'pro'
            }
        })

        console.log(response.data)
    } catch (error) {
        console.log(error)
    }
}

main()